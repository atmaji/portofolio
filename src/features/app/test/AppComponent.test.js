import { render, screen } from '@testing-library/react';
import AppComponent from '../components/AppComponent';

test('renders learn react link', () => {
  render(<AppComponent />);
  const linkElement = screen.getByText(/Hello/i);
  expect(linkElement).toBeInTheDocument();
});
