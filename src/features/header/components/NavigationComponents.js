import React, { Fragment } from "react"


const NavigationComponent = () => {
    return (
        <Fragment>
            <nav className="navbar sticky-top justify-content-between">
                <div className="container">
                    <div className="navbar-brand"><h3>test</h3></div>
                    <ul className="nav">
                        <li className="nav-item">
                            <div className="nav-link">Home</div>
                        </li>
                        <li className="nav-item">
                            <div className="nav-link">Project</div>
                        </li>
                        <li className="nav-item">
                            <div className="nav-link">Contact</div>
                        </li>
                    </ul>
                </div>
            </nav>
        </Fragment>
    )
}

export default NavigationComponent