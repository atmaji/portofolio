FROM docker.io/library/node:16.14.0-alpine3.14

COPY . .

RUN npm install

CMD ["npm", "start"]
